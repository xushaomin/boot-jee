package com.gitee.hermer.boot.jee.cache;


public interface CacheExpiredListener {

	
	public void notifyElementExpired(String region, Object key) ;

}
