package com.gitee.hermer.boot.jee.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitee.hermer.boot.jee.cache.redis.RedisCacheProvider;
import com.gitee.hermer.boot.jee.cache.redis.RedisCacheProxy;

import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.util.SafeEncoder;

import java.util.List;


public class RedisCacheChannel extends BinaryJedisPubSub implements CacheExpiredListener, CacheChannel {

    private final static Logger log = LoggerFactory.getLogger(RedisCacheChannel.class);

    private String name;
    private static String channel = BootCache.getConfig().getProperty("redis.channel_name");
    private final static RedisCacheChannel instance = new RedisCacheChannel("default");
    private final Thread thread_subscribe;

    private RedisCacheProxy redisCacheProxy;

    
    public final static RedisCacheChannel getInstance() {
        return instance;
    }

    
    private RedisCacheChannel(String name) throws CacheException {
        this.name = name;
        try {
            long ct = System.currentTimeMillis();
            CacheManager.initCacheProvider(this);
            redisCacheProxy = new RedisCacheProvider().getResource();
            thread_subscribe = new Thread(new Runnable() {
                @Override
                public void run() {
                    redisCacheProxy.subscribe(RedisCacheChannel.this, SafeEncoder.encode(channel));
                }
            });

            thread_subscribe.start();

            log.info("Connected to channel:" + this.name + ", time " + (System.currentTimeMillis() - ct) + " ms.");

        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    
    @Override
	public CacheObject get(String region, Object key) {
        CacheObject obj = new CacheObject();
        obj.setRegion(region);
        obj.setKey(key);
        if (region != null && key != null) {
            obj.setValue(CacheManager.get(LEVEL_1, region, key));
            if (obj.getValue() == null) {
                obj.setValue(CacheManager.get(LEVEL_2, region, key));
                if (obj.getValue() != null) {
                    obj.setLevel(LEVEL_2);
                    CacheManager.set(LEVEL_1, region, key, obj.getValue());
                }
            } else
                obj.setLevel(LEVEL_1);
        }
        return obj;
    }

    
    @Override
	public void set(String region, Object key, Object value) {
        if (region != null && key != null) {
            if (value == null)
                evict(region, key);
            else {
                
                
                
                
                
                
                
                
                CacheManager.set(LEVEL_1, region, key, value);
                CacheManager.set(LEVEL_2, region, key, value);
                _sendEvictCmd(region, key);
            }
        }
        
    }
    
    @Override
	public void set(String region, Object key, Object value, Integer expireInSec) {
        if (region != null && key != null) {
            if (value == null)
                evict(region, key);
            else {
                
                
                
                
                
                
                
                
                CacheManager.set(LEVEL_1, region, key, value, expireInSec);
                CacheManager.set(LEVEL_2, region, key, value, expireInSec);
                _sendEvictCmd(region, key);
            }
        }
        
    }

    
    @Override
	public void evict(String region, Object key) {
        CacheManager.evict(LEVEL_1, region, key); 
        CacheManager.evict(LEVEL_2, region, key); 
        _sendEvictCmd(region, key); 
    }

    
    @Override
	@SuppressWarnings({"rawtypes"})
    public void batchEvict(String region, List keys) {
        CacheManager.batchEvict(LEVEL_1, region, keys);
        CacheManager.batchEvict(LEVEL_2, region, keys);
        _sendEvictCmd(region, keys);
    }

    
    @Override
	public void clear(String region) throws CacheException {
        CacheManager.clear(LEVEL_1, region);
        CacheManager.clear(LEVEL_2, region);
        _sendClearCmd(region);
    }

    
    @Override
	@SuppressWarnings("rawtypes")
    public List keys(String region) throws CacheException {
        return CacheManager.keys(LEVEL_1, region);
    }

    
    @Override
    @SuppressWarnings("rawtypes")
    public void notifyElementExpired(String region, Object key) {

        log.debug("Cache data expired, region=" + region + ",key=" + key);

        
        if (key instanceof List)
            CacheManager.batchEvict(LEVEL_2, region, (List) key);
        else
            CacheManager.evict(LEVEL_2, region, key);

        
        _sendEvictCmd(region, key);
    }

    
    private void _sendEvictCmd(String region, Object key) {
        
        Command cmd = new Command(Command.OPT_DELETE_KEY, region, key);
        try {
            redisCacheProxy.publish(SafeEncoder.encode(channel), cmd.toBuffers());
        } catch (Exception e) {
            log.error("Unable to delete cache,region=" + region + ",key=" + key, e);
        }
    }

    
    private void _sendClearCmd(String region) {
        
        Command cmd = new Command(Command.OPT_CLEAR_KEY, region, "");
        try {
            redisCacheProxy.publish(SafeEncoder.encode(channel), cmd.toBuffers());
        } catch (Exception e) {
            log.error("Unable to clear cache,region=" + region, e);
        }
    }

    
    @SuppressWarnings("rawtypes")
    protected void onDeleteCacheKey(String region, Object key) {
        if (key instanceof List)
            CacheManager.batchEvict(LEVEL_1, region, (List) key);
        else
            CacheManager.evict(LEVEL_1, region, key);
        log.debug("Received cache evict message, region=" + region + ",key=" + key);
    }

    
    protected void onClearCacheKey(String region) {
        CacheManager.clear(LEVEL_1, region);
        log.debug("Received cache clear message, region=" + region);
    }

    
    @Override
    public void onMessage(byte[] channel, byte[] message) {
        
        if (message != null && message.length <= 0) {
            log.warn("Message is empty.");
            return;
        }

        try {
            Command cmd = Command.parse(message);

            if (cmd == null || cmd.isLocalCommand())
                return;

            switch (cmd.getOperator()) {
                case Command.OPT_DELETE_KEY:
                    onDeleteCacheKey(cmd.getRegion(), cmd.getKey());
                    break;
                case Command.OPT_CLEAR_KEY:
                    onClearCacheKey(cmd.getRegion());
                    break;
                default:
                    log.warn("Unknown message type = " + cmd.getOperator());
            }
        } catch (Exception e) {
            log.error("Unable to handle received msg", e);
        }
    }

    
    @Override
	public void close() {
        CacheManager.shutdown(LEVEL_1);
        if (isSubscribed()) {
            this.unsubscribe();
        }
        CacheManager.shutdown(LEVEL_2);
        
    }

}
