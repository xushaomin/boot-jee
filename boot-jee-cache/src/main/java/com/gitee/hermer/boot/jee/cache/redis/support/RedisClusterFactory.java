package com.gitee.hermer.boot.jee.cache.redis.support;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.gitee.hermer.boot.jee.cache.redis.client.ClusterRedisClient;


public class RedisClusterFactory implements RedisClientFactory<ClusterRedisClient> {

    private static ClusterRedisClient redisClient;
    private RedisPoolConfig poolConfig;

    private int maxRedirections = 0;

    private Pattern p = Pattern.compile("^.+[:]\\d{1,5}\\s*$");

    @Override
    public synchronized ClusterRedisClient getResource() {
        return redisClient;
    }

    
    @Override
    public void returnResource(ClusterRedisClient client) {

    }

    @Override
	public void build() {

        


        Set<HostAndPort> hostAndPorts = parseHostAndPort();
        if (maxRedirections == 0) { 
            maxRedirections = hostAndPorts.size();
        }

        redisClient = new ClusterRedisClient(
                new JedisCluster(hostAndPorts, poolConfig.getTimeout(), maxRedirections, poolConfig));
    }

    private Set<HostAndPort> parseHostAndPort() {

        String host = this.poolConfig.getHost();

        Set<HostAndPort> haps = new HashSet<HostAndPort>();
        String[] hosts = host.split(",");
        for (String val : hosts) {
            boolean isIpPort = p.matcher(val).matches();

            if (!isIpPort) {
                throw new IllegalArgumentException("ip or port is illegal.");
            }
            String[] ipAndPort = val.split(":");
            HostAndPort hap = new HostAndPort(ipAndPort[0], Integer.parseInt(ipAndPort[1]));
            haps.add(hap);
        }
        return haps;
    }

    public void setMaxRedirections(int maxRedirections) {
        this.maxRedirections = maxRedirections;
    }

    public void setPoolConfig(RedisPoolConfig poolConfig) {
        this.poolConfig = poolConfig;
    }

    public RedisPoolConfig getPoolConfig() {
        return this.poolConfig;
    }

    
    @Override
    public void close() throws IOException {
        redisClient.close();
    }
}
