package com.gitee.hermer.boot.jee.cache.utils;

import net.sf.ehcache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitee.hermer.boot.jee.cache.CacheManager;

import java.io.IOException;


public class SerializationUtils {

    private final static Logger log = LoggerFactory.getLogger(SerializationUtils.class);
    private static Serializer g_ser;


    static {
        String ser = CacheManager.getSerializer();
        if (ser == null || "".equals(ser.trim()))
            g_ser = new JavaSerializer();
        else {
            if (ser.equals("java")) {
                g_ser = new JavaSerializer();
            } else if (ser.equals("fst")) {
                g_ser = new FSTSerializer();
            } else if (ser.equals("kryo")) {
                g_ser = new KryoSerializer();
            } else if (ser.equals("kryo_pool_ser")){
            	g_ser = new KryoPoolSerializer();
            }else if(ser.equals("fst_snappy")){
            	g_ser=new FstSnappySerializer();
            }else {
                try {
                    g_ser = (Serializer) Class.forName(ser).newInstance();
                } catch (Exception e) {
                    throw new CacheException("Cannot initialize Serializer named [" + ser + ']', e);
                }
            }
        }
        log.info("Using Serializer -> [" + g_ser.name() + ":" + g_ser.getClass().getName() + ']');
    }

    public static byte[] serialize(Object obj) throws IOException {
        return g_ser.serialize(obj);
    }

    public static Object deserialize(byte[] bytes) throws IOException {
        return g_ser.deserialize(bytes);
    }

}
