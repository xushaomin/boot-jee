package com.gitee.hermer.boot.jee.commons.constants;

public class SystemPropertyConstant {
	
public final static String OS_NAME 				= "os.name";
	
	public final static String OS_VERSION			= "os.version";
	
	public final static String OS_ARCH 				= "os.arch";
	
	public final static String USER_LANGYAGE 		= "user.language";

}
