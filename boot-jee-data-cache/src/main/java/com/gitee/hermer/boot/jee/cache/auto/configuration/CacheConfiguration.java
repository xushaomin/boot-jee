package com.gitee.hermer.boot.jee.cache.auto.configuration;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.gitee.hermer.boot.jee.cache.BootCacheChannel;
import com.gitee.hermer.boot.jee.cache.CacheListener;
import com.gitee.hermer.boot.jee.cache.CacheManager;
import com.gitee.hermer.boot.jee.cache.CacheProvider;
import com.gitee.hermer.boot.jee.cache.SimpleCacheProvider;
import com.gitee.hermer.boot.jee.cache.ehcache.EhCacheProvider;
import com.gitee.hermer.boot.jee.cache.exception.CacheException;
import com.gitee.hermer.boot.jee.cache.listener.BootCacheListener;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;
import com.gitee.hermer.boot.jee.cache.redis.RedisProvider;
import com.gitee.hermer.boot.jee.cache.serializer.FSTSerializer;
import com.gitee.hermer.boot.jee.cache.serializer.FstSnappySerializer;
import com.gitee.hermer.boot.jee.cache.serializer.JavaSerializer;
import com.gitee.hermer.boot.jee.cache.serializer.KryoSerializer;
import com.gitee.hermer.boot.jee.cache.serializer.Serializer;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.commons.number.IntegerUtils;
import com.gitee.hermer.boot.jee.commons.number.NumberUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableConfigurationProperties(value={BootCacheProperties.class})
@AutoConfigureOrder(10)
public class CacheConfiguration extends UtilsContext{


	@Bean(destroyMethod="stop",name="l1_provider")
	@ConditionalOnMissingBean(EhCacheProvider.class)
	@ConditionalOnProperty(value="cache.L1.provider_class",havingValue="ehcache")
	public EhCacheProvider getCacheProvider(BootCacheProperties cacheProperties){
		EhCacheProvider provider = null;
		try{
			provider = new EhCacheProvider();
			provider.start(cacheProperties);
		}catch (Exception e) {
			error(e.getMessage(),e);
		}
		return provider;
	}
	
	@Bean(destroyMethod="close")
	@ConditionalOnMissingBean(BootCacheChannel.class)
	public BootCacheChannel getBootCacheChannel(){
		return new BootCacheChannel();
	}
	
	
	
	@Bean(destroyMethod="stop",name="l1_provider")
	@ConditionalOnMissingBean(name="l1_provider")
	@ConditionalOnProperty(value="cache.L1.provider_class",havingValue="none",matchIfMissing=true)
	public CacheProvider getSimpleL1CacheProvider(){
		return new SimpleCacheProvider();
	}
	
	@Bean(destroyMethod="stop",name="l2_provider")
	@ConditionalOnMissingBean(name="l2_provider")
	@ConditionalOnProperty(value="cache.L2.provider_class",havingValue="none",matchIfMissing=true)
	public CacheProvider getSimpleL2CacheProvider(){
		return new SimpleCacheProvider();
	}
	
	
	
	
	
	
	
	@Bean(destroyMethod="stop",name="l2_provider")
	@ConditionalOnMissingBean(RedisProvider.class)
	@ConditionalOnProperty(value="cache.L2.provider_class",havingValue="redis")
	public RedisProvider getRedisProvider(BootCacheProperties cacheProperties){
		RedisProvider provider = new RedisProvider();
		try {
			provider.start(cacheProperties);
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}
		return provider;
	}
	

	@Bean(destroyMethod="destroy")
	@ConditionalOnMissingBean(JedisConnectionFactory.class)
	@ConditionalOnProperty(value="cache.L2.provider_class",havingValue="redis")
	public JedisConnectionFactory getJedisConnectionFactory(BootCacheProperties cacheProperties){

		JedisConnectionFactory factory = null;
		JedisPoolConfig config = new JedisPoolConfig();
		if(StringUtils.isNotBlank(cacheProperties.getRedisMaxTotal()))
			config.setMaxTotal(NumberUtils.parseNumber(cacheProperties.getRedisMaxTotal(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisMaxIdle()))
			config.setMaxIdle(NumberUtils.parseNumber(cacheProperties.getRedisMaxIdle(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisMaxWaitMillis()))
			config.setMaxWaitMillis(NumberUtils.parseNumber(cacheProperties.getRedisMaxWaitMillis(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisMinEvictableIdleTimeMillis()))
			config.setMinEvictableIdleTimeMillis(NumberUtils.parseNumber(cacheProperties.getRedisMinEvictableIdleTimeMillis(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisMinIdle()))
			config.setMinIdle(NumberUtils.parseNumber(cacheProperties.getRedisMinIdle(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisNumTestsPerEvictionRun()))
			config.setNumTestsPerEvictionRun(NumberUtils.parseNumber(cacheProperties.getRedisNumTestsPerEvictionRun(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisLifo()))
			config.setLifo(BooleanUtils.toBoolean(cacheProperties.getRedisLifo()));
		if(StringUtils.isNotBlank(cacheProperties.getRedisSoftMinEvictableIdleTimeMillis()))
			config.setSoftMinEvictableIdleTimeMillis(NumberUtils.parseNumber(cacheProperties.getRedisSoftMinEvictableIdleTimeMillis(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisTestOnBorrow()))
			config.setTestOnBorrow(BooleanUtils.toBoolean(cacheProperties.getRedisTestOnBorrow()));
		if(StringUtils.isNotBlank(cacheProperties.getRedisTestOnReturn()))
			config.setTestOnReturn(BooleanUtils.toBoolean(cacheProperties.getRedisTestOnReturn()));
		if(StringUtils.isNotBlank(cacheProperties.getRedisTestWhileIdle()))
			config.setTestWhileIdle(BooleanUtils.toBoolean(cacheProperties.getRedisTestWhileIdle()));
		if(StringUtils.isNotBlank(cacheProperties.getRedisTimeBetweenEvictionRunsMillis()))
			config.setTimeBetweenEvictionRunsMillis(NumberUtils.parseNumber(cacheProperties.getRedisTimeBetweenEvictionRunsMillis(), Integer.class));
		if(StringUtils.isNotBlank(cacheProperties.getRedisblockWhenExhausted()))
			config.setBlockWhenExhausted(BooleanUtils.toBoolean(cacheProperties.getRedisblockWhenExhausted()));

		if(("cluster").equals(cacheProperties.getRedisPolicy())){
			RedisClusterConfiguration configuration = new RedisClusterConfiguration(CollectionUtils.newHashSet(StringUtils.split(cacheProperties.getRedisHost(), ",")));
			factory = new JedisConnectionFactory(configuration, config);
			
		}else if (("single").equals(cacheProperties.getRedisPolicy())) {
			factory = new JedisConnectionFactory(config);
			factory.setHostName(cacheProperties.getRedisHost());
			factory.setPort(IntegerUtils.defaultIfError(cacheProperties.getRedisPort(), 6379));
		}
		
		factory.setDatabase(IntegerUtils.defaultIfError(NumberUtils.parseNumber(cacheProperties.getRedisDataBase(), Integer.class), 1));
		factory.setTimeout(IntegerUtils.defaultIfError(NumberUtils.parseNumber(cacheProperties.getRedisTimeOut(), Integer.class), 2000));
		factory.setPassword(cacheProperties.getRedisPassword());
		return factory;
	}
	
	
	@Bean
	@ConditionalOnProperty(value = "cache.serialization",havingValue="fst")
	@ConditionalOnMissingBean(FSTSerializer.class)
	public Serializer getFSTSerializer(){
		return new FSTSerializer();
	}
	
	@Bean
	@ConditionalOnProperty(value = "cache.serialization",havingValue="java")
	@ConditionalOnMissingBean(JavaSerializer.class)
	public Serializer getJavaSerializer(){
		return new JavaSerializer();
	}
	
	@Bean
	@ConditionalOnProperty(value = "cache.serialization",havingValue="kryo")
	@ConditionalOnMissingBean(KryoSerializer.class)
	public Serializer getKryoSerializer(){
		return new KryoSerializer();
	}
	
	@Bean
	@ConditionalOnProperty(value = "cache.serialization",havingValue="fst_snappy")
	@ConditionalOnMissingBean(FstSnappySerializer.class)
	public Serializer getFstSnappySerializer(){
		return new FstSnappySerializer();
	}
	
	
	@Bean(name="redisClient")
	@ConditionalOnMissingBean(RedisTemplate.class)
	@ConditionalOnProperty(value="cache.L2.provider_class",havingValue="redis")
	public RedisTemplate getRedisTemplate(JedisConnectionFactory factory,Serializer serializer){
		RedisTemplate<String,Object> template = new RedisTemplate<String,Object>();
		template.setEnableDefaultSerializer(true);
		template.setDefaultSerializer(serializer);
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new StringRedisSerializer());
		template.setConnectionFactory(factory);
		template.afterPropertiesSet();
		return template;
	}
	
	@Bean
	@ConditionalOnMissingBean(CacheManager.class)
	public CacheManager getCacheManager(){
		return new CacheManager();
	}
	
	@Bean
	@ConditionalOnMissingBean(CacheListener.class)
	public CacheListener getCacheListener(){
		return new BootCacheListener();
	}
	
	
	@Bean(destroyMethod="destroy")
	public RedisMessageListenerContainer getListenerContainer(JedisConnectionFactory factory,BootCacheChannel channel,BootCacheProperties properties){
		RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(factory);
		ThreadPoolTaskScheduler eScheduler = new ThreadPoolTaskScheduler();
		eScheduler.setPoolSize(3);
		container.setTaskExecutor(eScheduler);
		container.addMessageListener(channel, new ChannelTopic(properties.getRedisChannelName()));
		return container;
		
	}
	
	
	


}
