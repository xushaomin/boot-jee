package com.gitee.hermer.boot.jee.cache.listener;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.env.PropertySourcesLoader;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;

public class BootCacheEnvironmentListener extends UtilsContext implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

	private ResourceLoader loader = new DefaultResourceLoader();  	

	@Override
	public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
		try{
			Resource resource = loader.getResource("classpath:boot-cache.properties");  
			PropertySource<?> propertySource = new PropertySourcesLoader().load(resource);  
			event.getEnvironment().getPropertySources().addLast(propertySource);  
		}catch (Exception e) {
			error(e.getMessage(),e);
		}

	}

}
