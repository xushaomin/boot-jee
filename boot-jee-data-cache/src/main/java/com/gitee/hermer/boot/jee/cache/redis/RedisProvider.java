
package com.gitee.hermer.boot.jee.cache.redis;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;

import com.gitee.hermer.boot.jee.cache.CacheListener;
import com.gitee.hermer.boot.jee.cache.CacheProvider;
import com.gitee.hermer.boot.jee.cache.exception.CacheException;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;


public class RedisProvider extends UtilsContext implements CacheProvider {

	private ConcurrentHashMap<String, RedisCacheClient> cacheManager ;
	
	
	private BootCacheProperties properties;
	
	@Autowired
	@Qualifier("redisClient")
	private RedisTemplate client;


	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "redis";
	}

	@Override
	public RedisCacheClient buildCache(String name, boolean autoCreate, CacheListener listener) throws CacheException {
		RedisCacheClient redcache = cacheManager.get(name);
		if(redcache == null && autoCreate){
			try {
				synchronized(cacheManager){
					redcache = new RedisCacheClient(name,client,properties, listener);
					cacheManager.put(name, redcache);
				}
			}
			catch (Exception e) {
				throw new CacheException(e);
			}
		}
		return redcache;
	}

	@Override
	public void start(BootCacheProperties properties) throws CacheException {
		cacheManager = new ConcurrentHashMap<String, RedisCacheClient>();
		this.properties = properties;

	}

	@Override
	public void stop() {
		if(cacheManager != null)
			cacheManager.clear();
		cacheManager = null;

	}


}
