package com.gitee.hermer.boot.jee.cache.serializer;

import java.io.Serializable;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;



public interface Serializer extends RedisSerializer<Serializable>{
	
	public String name();

	@Override
	Serializable deserialize(byte[] bytes) throws SerializationException;
	
	@Override
	byte[] serialize(Serializable t) throws SerializationException;
	
	
}
