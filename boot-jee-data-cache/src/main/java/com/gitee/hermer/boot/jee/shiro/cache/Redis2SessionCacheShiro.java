package com.gitee.hermer.boot.jee.shiro.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;

import com.gitee.hermer.boot.jee.cache.BootCacheChannel;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.log.Logger;
import com.gitee.hermer.boot.jee.commons.number.IntegerUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

public class Redis2SessionCacheShiro extends AbstractSessionDAO{
	
	private String  keyPrefix = "boot-cache-shiro";  
	private int sessionTimeOut=30*60;
	
	private Logger log = Logger.getLogger(getClass());
	
	public Redis2SessionCacheShiro(){
		
	}
	
	public Redis2SessionCacheShiro(String prefix,Integer sessionTimeOut){
		if(StringUtils.isNotEmpty(prefix))
			this.keyPrefix = prefix;
		if(IntegerUtils.isBiggerThan0(sessionTimeOut))
			this.sessionTimeOut = sessionTimeOut;
	}
	
	
	@Autowired
	private BootCacheChannel channel;
	

	@Override
	public void delete(Session arg0) {
		log.debug(StringUtils.format("删除seesion,id=[{}]", arg0.getId().toString()));  
		channel.evict(keyPrefix, arg0.getId());
	}

	@Override
	public Collection<Session> getActiveSessions() {
		List<String> keys = channel.keys(keyPrefix);
		Collection<Session> sessions = CollectionUtils.newArrayList();
		for (String key : keys) {
			sessions.add((Session)channel.get(keyPrefix, key.substring(key.lastIndexOf(":")+1)));
		}
		return sessions;
	}

	@Override
	public void update(Session arg0) throws UnknownSessionException {
		log.debug(StringUtils.format("更新seesion,id=[{}]", arg0.getId().toString()));  
		channel.set(keyPrefix, arg0.getId(), arg0,sessionTimeOut);
	}

	@Override
	protected Serializable doCreate(Session arg0) {
		Serializable sessionId = generateSessionId(arg0);  
        assignSessionId(arg0, sessionId);  
        log.debug(StringUtils.format("创建seesion,id=[{}]", arg0.getId().toString()));  
        channel.set(keyPrefix, arg0.getId(), arg0,sessionTimeOut);
		return null;
	}

	@Override
	protected Session doReadSession(Serializable arg0) {
		log.debug(StringUtils.format("获取seesion,id=[{}]", arg0.toString()));  
        Session readSession = channel.get(keyPrefix, arg0);
		return readSession;
	}
	

}
