package com.gitee.hermer.boot.jee.orm.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.boot.jee.orm.page")  
public class OrmPageProperties {
	private String offsetAsPageNum = "true";
	private String rowBoundsWithCount = "true";
	private String reasonable = "true";
	private String dialect = "mysql";
	public String getOffsetAsPageNum() {
		return offsetAsPageNum;
	}
	public void setOffsetAsPageNum(String offsetAsPageNum) {
		this.offsetAsPageNum = offsetAsPageNum;
	}
	public String getRowBoundsWithCount() {
		return rowBoundsWithCount;
	}
	public void setRowBoundsWithCount(String rowBoundsWithCount) {
		this.rowBoundsWithCount = rowBoundsWithCount;
	}
	public String getReasonable() {
		return reasonable;
	}
	public void setReasonable(String reasonable) {
		this.reasonable = reasonable;
	}
	public String getDialect() {
		return dialect;
	}
	public void setDialect(String dialect) {
		this.dialect = dialect;
	}	
}
