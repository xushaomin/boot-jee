package com.gitee.hermer.boot.jee.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;



public class BaseControllerHandler extends UtilsContext{
	
	@Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    protected String getProtocolVersion()
    {
        return request.getProtocol();
    }
    protected void writeError(Throwable e){ 
    	error(e.getMessage(),e);
    }
   

   
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handleCFException(Throwable e){
    	writeError(e);
    	ModelAndView model = new ModelAndView(StringUtils.defaultIfEmpty(getDictValueString("com.boot.jee.web.500.page"), "erorr/500"));
    	model.addObject("error", e.getMessage());
    	return model;
    }
    
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ModelAndView handleNotFoundException(HttpServletRequest request,
    		NoHandlerFoundException ex) {
    	ModelAndView model = new ModelAndView(StringUtils.defaultIfEmpty(getDictValueString("com.boot.jee.web.404.page"), "erorr/404"));
    	writeError(ex);
    	return model;
    }
  
}
